/**************************************************************************
 *   comment.rs  --  This file is part of ctranslator.                    *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::tokenize::remove_first;

pub fn translate_comment(code_: &[Vec<&str>]) -> String {
    let mut code = code_.to_owned();
    let mut output: String = String::new();
    for i in 1..code.len() - 1 {
        output.push_str("///");
        if let Some(line) = remove_first(code[i][1]) {
            code[i][1] = line;
            output.push_str(line);
            output.push_str(&code[i].join(" "));
        }
        output.push('\n')
    }
    output
}
