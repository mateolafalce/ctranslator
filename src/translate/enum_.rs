/**************************************************************************
 *   enum_.rs  --  This file is part of ctranslator.                      *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::tokenize::{delete_initial_space, remove_char};

pub fn translate_enum(code: &Vec<Vec<&str>>) -> String {
    let mut enum_: String = String::new();
    let mut iter: u8 = 0;
    for code_line in code {
        let mut line = code_line.join(" ");
        if line.len() >= 9 && line.contains('=') && line.contains("<<") {
            enum_ = enum_
                + "const "
                + code_line[4]
                + ": usize = "
                + code_line[6]
                + " << "
                + &remove_char(code_line[8], ',')
                + ";\n";
        }
        if line.contains("/*") {
            line.pop();
            line.pop();
            enum_ = enum_ + "///" + &delete_initial_space(&line) + "\n";
        }
        if !line.contains('=') && !line.contains("enum") && !line.contains("/*") && line.len() >= 5
        {
            enum_ = enum_
                + "const "
                + &remove_char(code_line[4], ',')
                + ": usize = "
                + &iter.to_string()
                + ";\n";
            iter += 1;
        }
    }
    enum_
}
