/**************************************************************************
 *   line.rs  --  This file is part of ctranslator.                       *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::tokenize::{delete_initial_space, remove_char, remove_first, to_upper_cammel_case};

/// Returns a translated text
pub fn translate_line(line: Vec<&str>) -> String {
    if let Some(i) = (4..line.len()).next() {
        match line[i] {
            "int" => return line[i + 1].to_owned() + ": i32,\n",
            "u8" => {
                if line.len() > i + 1 && line[i + 1].contains(":1") {
                    let a = remove_char(line[i + 1], ':');
                    let b = remove_char(&a, '1');
                    return b.to_owned() + ": bool,\n";
                } else if line.len() > i + 1 && line[i + 1].contains(":3") {
                    let line_a = remove_char(line[i + 1], ':');
                    let line_b = remove_char(&line_a, '3');
                    return line_b.to_owned() + ": u8,\n";
                } else {
                    return line[i + 1].to_owned() + ": u8,\n";
                }
            }
            "u16" => return line[i + 1].to_owned() + ": u16,\n",
            "u32" => {
                if line[i + 1].contains('*') {
                    return remove_first(line[i + 1]).unwrap().to_owned() + ": *mut u32,\n";
                } else {
                    return line[i + 1].to_owned() + ": u32,\n";
                }
            }
            "u64" => {
                if line[i + 1].contains('*') {
                    return remove_first(line[i + 1]).unwrap().to_owned() + ": *mut u64,\n";
                } else {
                    return line[i + 1].to_owned() + ": u64,\n";
                }
            }
            "char" => return line[i + 1].to_owned() + ": char,\n",
            "bool" => return line[i + 1].to_owned() + ": bool,\n",
            "unsigned" => match line[i + 1] {
                "char" => {
                    if line.len() == i + 2 {
                        return line[i + 2].to_owned() + ": u8,\n";
                    } else {
                        return line[i + 2].to_owned() + ": [u8;" + line[i + 3] + "],\n";
                    }
                }
                "int" => {
                    if line.len() > i + 2 && !line[i + 2].contains(":1") {
                        return line[i + 2].to_owned() + ": u32,\n";
                    } else if line.len() > i + 2 && line[i + 2].contains(":1") {
                        let a = remove_char(line[i + 2], ':');
                        let b = remove_char(&a, '1');
                        return b.to_owned() + ": bool,\n";
                    } else {
                        return "".to_string();
                    }
                }
                "long" => return line[i + 2].to_owned() + ": u64,\n",
                "short" => return line[i + 2].to_owned() + ": u16,\n",
                _ => return line[i + 1].to_owned() + ": u32,\n",
            },
            "struct" => {
                if line.len() > i + 3 && line[i + 2].contains('*') && !line[i + 3].contains('>') {
                    return remove_first(line[i + 2]).unwrap().to_owned()
                        + ": *mut "
                        + &to_upper_cammel_case(line[i + 1])
                        + ",\n";
                } else if line.len() > i + 3
                    && line[i + 2].contains('*')
                    && line[i + 3].contains('>')
                {
                    return remove_first(line[i + 2]).unwrap().to_owned()
                        + ": [*mut "
                        + &to_upper_cammel_case(line[i + 1])
                        + ";10],\n";
                }
                if line.len() >= i + 3 && line[i + 2].contains('*') {
                    return remove_first(line[i + 2]).unwrap().to_owned()
                        + ": *mut "
                        + &to_upper_cammel_case(line[i + 1])
                        + ",\n";
                } else if line.len() > i + 3 && line[i + 3].contains('>') {
                    return remove_first(line[i + 2]).unwrap().to_owned()
                        + ": ["
                        + &to_upper_cammel_case(line[i + 1])
                        + ";10],\n";
                } else if line.len() >= 8 && !line[i + 3].contains("/*") {
                    return line[i + 2].to_owned()
                        + ": ["
                        + &to_upper_cammel_case(line[i + 1])
                        + ";"
                        + line[i + 3]
                        + "],\n";
                } else {
                    return line[i + 2].to_owned()
                        + ": "
                        + &to_upper_cammel_case(line[i + 1])
                        + ",\n";
                }
            }
            "const" => match line[i + 1] {
                "char" => {
                    return remove_first(line[i + 2]).unwrap().to_owned() + ": *const char,\n"
                }
                "struct" => {
                    return remove_first(line[i + 3]).unwrap().to_owned()
                        + ": *const "
                        + &to_upper_cammel_case(line[i + 2])
                        + ",\n"
                }
                _ => {
                    return remove_first(line[i + 2]).unwrap().to_owned()
                        + ": *const "
                        + &to_upper_cammel_case(line[i + 1])
                        + ",\n"
                }
            },
            "*" => {
                return "///".to_owned() + &delete_initial_space(&line.join(" ")) + "\n";
            }
            "/*" => {
                let mut delete_end_comment = line.clone();
                delete_end_comment.pop();
                let deleted: String = delete_end_comment.join(" ");
                return "///".to_owned() + &delete_initial_space(&deleted) + "\n";
            }
            "enum" => return "// TODO impl enum ".to_owned() + line[i + 1] + " \n",
            _ => {
                if line.len() > i + 1 && line[i + 1].contains('*') {
                    return remove_first(line[i + 1]).unwrap().to_owned()
                        + ": *mut "
                        + &to_upper_cammel_case(line[i])
                        + ",\n";
                } else if line.len() > i + 2
                    && line[i + 1].contains("__")
                    && line[i + 2].contains('*')
                {
                    return remove_first(line[i + 2]).unwrap().to_owned()
                        + ": *mut "
                        + &to_upper_cammel_case(line[i])
                        + ",\n";
                } else if line.len() > i + 1 {
                    return line[i + 1].to_owned() + ": " + &to_upper_cammel_case(line[i]) + ",\n";
                } else {
                    return "".to_string();
                }
            }
        };
    }
    "".to_string()
}
