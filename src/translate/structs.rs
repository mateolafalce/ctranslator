/**************************************************************************
 *   structs.rs  --  This file is part of ctranslator.                    *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{tokenize::to_upper_cammel_case, translate::line::translate_line};

pub fn translate_struct(code: &[Vec<&str>]) -> String {
    let mut output: String = String::new();
    output.push_str(code[0][0]);
    output.push(' ');
    output.push_str(&to_upper_cammel_case(code[0][1]));
    output.push_str(" {\n");
    for code_line in code.iter().take(code.len() - 1).skip(1) {
        let tab: String = "    ".to_string();
        let translate = &translate_line(code_line.to_vec());
        if !translate.is_empty() {
            let line: String = tab + &translate_line(code_line.to_vec());
            output.push_str(&line);
        }
    }
    output.push('}');
    output
}
