/**************************************************************************
 *   tokenize.rs  --  This file is part of ctranslator.                   *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// Return tokenized code
pub fn get_code_token(code: &str) -> Vec<Vec<&str>> {
    let mut token: Vec<Vec<&str>> = vec![];
    let lines: Vec<&str> = code.split('\n').collect();
    for line in lines {
        let split_space = line.split(' ').collect();
        token.push(split_space);
    }
    token
}

/// From unsigned_integer to UnsignedInteger
pub fn to_upper_cammel_case(name: &str) -> String {
    // for __variable
    let mut name_char: Vec<char> = name.chars().collect();
    if name_char[0] == '_' && name_char[1] == '_' {
        name_char.remove(0);
        name_char.remove(0);
    }
    let name_fixed = name_char.iter().collect::<String>();
    let mut token: Vec<String> = name_fixed.split('_').map(|s| s.to_string()).collect();
    let token_len: usize = token.len();
    for token_line in token.iter_mut().take(token_len) {
        let mut to_char: Vec<char> = token_line.chars().collect();
        let upper = to_char[0].to_ascii_uppercase();
        to_char[0] = upper;
        *token_line = to_char.iter().collect::<String>();
    }
    let name: String = token.join("");
    name
}

/// Remove an specific char from the code
pub fn remove_char(code: &str, ch: char) -> String {
    let result: String = code.chars().filter(|&c| c != ch).collect();
    result
}

/// Replace specific chars in the current
pub fn replace_chars(input: &str) -> String {
    let for_array: String = input.to_string();
    let for_dyn_arr = for_array.replace("[]", " >");
    let for_tab = for_dyn_arr.replace('\t', "    ");
    let for_ob = for_tab.replace('[', " ");
    for_ob.replace(']', " ")
}

/// Delete first char of the code
pub fn remove_first(code: &str) -> Option<&str> {
    return code.chars().next().map(|c| &code[c.len_utf8()..]);
}

/// Delete initial tabulation
pub fn delete_initial_space(line: &str) -> String {
    let a = remove_first(line).unwrap();
    let b = remove_first(a).unwrap();
    let c = remove_first(b).unwrap();
    let d = remove_first(c).unwrap();
    let e = remove_first(d).unwrap();
    let f = remove_first(e).unwrap();
    f.to_string()
}
