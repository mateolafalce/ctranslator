/**************************************************************************
 *   main.rs  --  This file is part of ctranslator.                       *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

pub mod error;
pub mod tokenize;
pub mod translate;

use crate::{
    error::{ErrorCT, ERR_GET_CODE, ERR_GET_PATH},
    tokenize::{get_code_token, remove_char, replace_chars},
    translate::{comment::translate_comment, enum_::translate_enum, structs::translate_struct},
};
use std::{env, fs::read_to_string, process::exit};

fn main() -> Result<(), ErrorCT> {
    // get file from argument
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("{}", ErrorCT::CustomError(ERR_GET_PATH));
        exit(1);
    }
    let path: &str = &args[1];
    // get code
    let result = read_to_string(path);
    match result {
        Ok(code) => {
            let replace_semicolons = remove_char(&code, ';');
            let replace_curls = replace_chars(&replace_semicolons);
            let code_token = get_code_token(&replace_curls);
            match code_token[0][0] {
                "struct" => println!("{}", translate_struct(&code_token)),
                "/*" | "/**" => println!("{}", translate_comment(&code_token)),
                "enum" => println!("{}", translate_enum(&code_token)),
                _ => (),
            }
        }
        Err(_) => {
            println!("{}", ErrorCT::CustomError(ERR_GET_CODE));
            exit(1);
        }
    }
    Ok(())
}
