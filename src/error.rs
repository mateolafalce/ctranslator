/**************************************************************************
 *   error.rs  --  This file is part of ctranslator.                      *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use std::fmt;

pub const ERR_GET_CODE: &str = "Loading of the file passed by argument, verify that it can be read";
pub const ERR_GET_PATH: &str = "Usage <file>";

#[derive(Debug)]
pub enum ErrorCT {
    CustomError(&'static str),
}

impl fmt::Display for ErrorCT {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ErrorCT::CustomError(msg) => {
                write!(f, "Error: {}", msg)
            }
        }
    }
}
