/**************************************************************************
 *   test.rs  --  This file is part of ctranslator.                       *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   ctranslator is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   ctranslator is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use ctranslator::tokenize::{
    delete_initial_space, get_code_token, remove_char, remove_first, replace_chars,
    to_upper_cammel_case,
};

#[test]
fn remove_first_works() {
    let a = remove_first("Test");
    assert_eq!(a, Some("est"));
}

#[test]
fn delete_initial_space_works() {
    let a = delete_initial_space("      struct...");
    assert_eq!(a, "struct...");
}

#[test]
fn replace_chars_works() {
    let a = replace_chars("[]\t [ ]");
    assert_eq!(a, " >        ");
}

#[test]
fn remove_char_works() {
    let a = remove_char("struct *", '*');
    assert_eq!(a, "struct ");
}

#[test]
fn to_upper_cammel_case_works() {
    let a = to_upper_cammel_case("unsigned_integer");
    assert_eq!(a, "UnsignedInteger");
}

#[test]
fn get_code_token_works() {
    let a = get_code_token("struct code\nint u8;");
    assert_eq!(a[0][0], "struct");
}
